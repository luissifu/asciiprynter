import sys
import math
import Image

class ConsoleImage():
  def __init__(self, src='test/test.jpg', tx=80, ty=12):
    self.color_chars = [" "," "," "," "," "," "," "," "," "," "]
    self.color_list = [
      (0,0,0),
      (128,0,0),
      (0,128,0),
      (128,128,0),
      (0,0,128),
      (128,0,128),
      (0,128,128),
      (192,192,192),
      (128,128,128),
      (255,0,0),
      (0,255,0),
      (255,255,0),
      (0,0,255),
      (255,0,255),
      (0,255,255),
      (255,255,255),
      (0,0,0),
      (0,0,95),
      (0,0,135),
      (0,0,175),
      (0,0,215),
      (0,0,255),
      (0,95,0),
      (0,95,95),
      (0,95,135),
      (0,95,175),
      (0,95,215),
      (0,95,255),
      (0,135,0),
      (0,135,95),
      (0,135,135),
      (0,135,175),
      (0,135,215),
      (0,135,255),
      (0,175,0),
      (0,175,95),
      (0,175,135),
      (0,175,175),
      (0,175,215),
      (0,175,255),
      (0,215,0),
      (0,215,95),
      (0,215,135),
      (0,215,175),
      (0,215,215),
      (0,215,255),
      (0,255,0),
      (0,255,95),
      (0,255,135),
      (0,255,175),
      (0,255,215),
      (0,255,255),
      (95,0,0),
      (95,0,95),
      (95,0,135),
      (95,0,175),
      (95,0,215),
      (95,0,255),
      (95,95,0),
      (95,95,95),
      (95,95,135),
      (95,95,175),
      (95,95,215),
      (95,95,255),
      (95,135,0),
      (95,135,95),
      (95,135,135),
      (95,135,175),
      (95,135,215),
      (95,135,255),
      (95,175,0),
      (95,175,95),
      (95,175,135),
      (95,175,175),
      (95,175,215),
      (95,175,255),
      (95,215,0),
      (95,215,95),
      (95,215,135),
      (95,215,175),
      (95,215,215),
      (95,215,255),
      (95,255,0),
      (95,255,95),
      (95,255,135),
      (95,255,175),
      (95,255,215),
      (95,255,255),
      (135,0,0),
      (135,0,95),
      (135,0,135),
      (135,0,175),
      (135,0,215),
      (135,0,255),
      (135,95,0),
      (135,95,95),
      (135,95,135),
      (135,95,175),
      (135,95,215),
      (135,95,255),
      (135,135,0),
      (135,135,95),
      (135,135,135),
      (135,135,175),
      (135,135,215),
      (135,135,255),
      (135,175,0),
      (135,175,95),
      (135,175,135),
      (135,175,175),
      (135,175,215),
      (135,175,255),
      (135,215,0),
      (135,215,95),
      (135,215,135),
      (135,215,175),
      (135,215,215),
      (135,215,255),
      (135,255,0),
      (135,255,95),
      (135,255,135),
      (135,255,175),
      (135,255,215),
      (135,255,255),
      (175,0,0),
      (175,0,95),
      (175,0,135),
      (175,0,175),
      (175,0,215),
      (175,0,255),
      (175,95,0),
      (175,95,95),
      (175,95,135),
      (175,95,175),
      (175,95,215),
      (175,95,255),
      (175,135,0),
      (175,135,95),
      (175,135,135),
      (175,135,175),
      (175,135,215),
      (175,135,255),
      (175,175,0),
      (175,175,95),
      (175,175,135),
      (175,175,175),
      (175,175,215),
      (175,175,255),
      (175,215,0),
      (175,215,95),
      (175,215,135),
      (175,215,175),
      (175,215,215),
      (175,215,255),
      (175,255,0),
      (175,255,95),
      (175,255,135),
      (175,255,175),
      (175,255,215),
      (175,255,255),
      (215,0,0),
      (215,0,95),
      (215,0,135),
      (215,0,175),
      (215,0,215),
      (215,0,255),
      (215,95,0),
      (215,95,95),
      (215,95,135),
      (215,95,175),
      (215,95,215),
      (215,95,255),
      (215,135,0),
      (215,135,95),
      (215,135,135),
      (215,135,175),
      (215,135,215),
      (215,135,255),
      (215,175,0),
      (215,175,95),
      (215,175,135),
      (215,175,175),
      (215,175,215),
      (215,175,255),
      (215,215,0),
      (215,215,95),
      (215,215,135),
      (215,215,175),
      (215,215,215),
      (215,215,255),
      (215,255,0),
      (215,255,95),
      (215,255,135),
      (215,255,175),
      (215,255,215),
      (215,255,255),
      (255,0,0),
      (255,0,95),
      (255,0,135),
      (255,0,175),
      (255,0,215),
      (255,0,255),
      (255,95,0),
      (255,95,95),
      (255,95,135),
      (255,95,175),
      (255,95,215),
      (255,95,255),
      (255,135,0),
      (255,135,95),
      (255,135,135),
      (255,135,175),
      (255,135,215),
      (255,135,255),
      (255,175,0),
      (255,175,95),
      (255,175,135),
      (255,175,175),
      (255,175,215),
      (255,175,255),
      (255,215,0),
      (255,215,95),
      (255,215,135),
      (255,215,175),
      (255,215,215),
      (255,215,255),
      (255,255,0),
      (255,255,95),
      (255,255,135),
      (255,255,175),
      (255,255,215),
      (255,255,255),
      (8,8,8),
      (18,18,18),
      (28,28,28),
      (38,38,38),
      (48,48,48),
      (58,58,58),
      (68,68,68),
      (78,78,78),
      (88,88,88),
      (96,96,96),
      (12,12,12),
      (118,118,118),
      (128,128,128),
      (138,138,138),
      (148,148,148),
      (158,158,158),
      (168,168,168),
      (178,178,178),
      (188,188,188),
      (198,198,198),
      (28,28,28),
      (218,218,218),
      (228,228,228),
      (238,238,238)
    ]
    self.src = src
    self.tx = tx
    self.ty = ty

  def colorprint(self, char, index):
    color_text = "\033[48;5;%dm%s\033[0m" % (index, char)
    sys.stdout.write(color_text)

  def getcolor(self, r,g,b):
    index = 0
    minrgb = 255

    for i in range(0, len(self.color_list)):
      mr = abs(self.color_list[i][0] - r)
      mg = abs(self.color_list[i][1] - g)
      mb = abs(self.color_list[i][2] - b)

      m = mr+mg+mb

      if m < minrgb:
        minrgb = m
        index = i

    return index

  def display(self):
    try:
      img = Image.open(self.src)
    except IOError:
      print "ERROR: '%s' not found" % self.src
      return

    size_x, size_y = img.size
    x = size_x/self.tx if size_x/self.tx > 1 else 1
    y = size_y/self.ty if size_y/self.ty > 1 else 1

    datalist = list(img.getdata())

    j = 0
    while j < size_y:
      i = 0
      while i < size_x:
        avg_r = avg_g = avg_b = 0.0

        for a in range (i,i+x):
          for b in range (j,j+y):
            try:
              avg_r += datalist[a+b*size_x][0]
              avg_g += datalist[a+b*size_x][1]
              avg_b += datalist[a+b*size_x][2]
            except IndexError:
              avg_r += 255
              avg_g += 255
              avg_b += 255

        avg_r = (avg_r/(x*y))
        avg_g = (avg_g/(x*y))
        avg_b = (avg_b/(x*y))

        lum = 0.2126 * avg_r/255 + 0.7152 * avg_g/255 + 0.0722 * avg_b/255

        char = int(math.floor(lum*9))
        self.colorprint(self.color_chars[char],self.getcolor(avg_r,avg_g,avg_b))
        i += x
      print ""
      j += y
