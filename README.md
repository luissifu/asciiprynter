Img2t
===

Un script de python para imprimir imagenes en consola (con colores!)

### Dependencias
* Python 2.7

### Utilizacion
```sh
$ img2t imagen.extension
```
Estas son las extensiones validas:
* jpg
* png

### Instalacion
1. Clonar el repo
```sh
$ git clone https://luissifu@bitbucket.org/luissifu/asciiprynter.git
```
2. (Opcional) Agregar al path
```sh
$ sudo ln -s /ubicacion/del/repo/img2t.py /usr/local/bin/img2t
```
