#!/usr/bin/env python
# encoding utf-8
import sys
from console import ConsoleImage

def usage():
  print 'Usage:'
  print '\timg2t [Image] [WidthxHeight]'
  print ''
  sys.exit()

argc = len(sys.argv)

if argc > 3:
  print 'ERROR: Incorrect number of arguments'
  usage()
elif argc == 1:
  img = ConsoleImage()
elif argc == 2:
  img = ConsoleImage(sys.argv[1])
elif argc == 3:
  tx, ty = sys.argv[2].split('x')
  img = ConsoleImage(sys.argv[1], int(tx), int(ty))
img.display()
